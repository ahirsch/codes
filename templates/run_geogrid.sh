#!/bin/bash
#PBS -l mem=4GB
#PBS -l ncpus=1
#PBS -l walltime=2:00:00
#PBS -q normal
#PBS -l wd
#PBS -W umask=0022
#PBS -l storage=gdata/sx70+gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}

ln -sf ${CODEDIR_ROOT}/WPS/geogrid . 
${CODEDIR_ROOT}/installs/${EXE_DIR}/WPS/geogrid.exe 

